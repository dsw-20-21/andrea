-- 1.- Creamos la base de datos
DROP DATABASE IF EXISTS project;
CREATE DATABASE project DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- Seleccionamos la base de datos "project"
USE project;

-- 2.- Creamos las tablas

-- 2.1.1.- Tabla SHOPS
DROP TABLE IF EXISTS `project`.`shops`;
CREATE TABLE IF NOT EXISTS shops (
 id INT AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(100) NOT NULL,
 phone VARCHAR(13) NULL
);
-- 2.1.2 .- Tabla families
DROP TABLE IF EXISTS `project`.`families`;
CREATE TABLE IF NOT EXISTS families (
 code VARCHAR(6) PRIMARY KEY,
 name VARCHAR(200) not null
);
-- 2.1.3.- Tabla products
DROP TABLE IF EXISTS `project`.`products`;
CREATE TABLE IF NOT EXISTS products (
 id INT AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(200) NOT NULL,
 short_name VARCHAR(50) UNIQUE NOT NULL,
 description TEXT NULL,
 price DECIMAL(10, 2) NOT NULL,
 family_code VARCHAR(6) NOT NULL,
 CONSTRAINT fk_prod_fam FOREIGN KEY(family_code) REFERENCES families(code) ON UPDATE CASCADE ON DELETE CASCADE
);
-- 2.1.4 Tabla stock
DROP TABLE IF EXISTS `project`.`stock`;
CREATE TABLE IF NOT EXISTS stock (
 product_id INT,
 shop_id INT,
 units INT UNSIGNED NOT NULL,
 CONSTRAINT pk_stock PRIMARY KEY(product_id, shop_id),
 CONSTRAINT fk_stock_prod FOREIGN KEY(product_id) REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE,
 CONSTRAINT fk_stock_tienda FOREIGN KEY(shop_id) REFERENCES shops(id) ON UPDATE CASCADE ON DELETE CASCADE
);
-- 2.1.5 Tabla users
DROP TABLE IF EXISTS `project`.`users`;
CREATE TABLE `project`.`users` (`username` VARCHAR(30) NOT NULL , `password` VARCHAR(32) NOT NULL, PRIMARY KEY (`username`)) ENGINE = InnoDB;

-- 3.- Creamos el usuario manager
CREATE USER IF NOT EXISTS 'manager'@'localhost' IDENTIFIED BY "secret";

-- 4.- Le damos permiso en la base de datos "project"
GRANT ALL ON project.* to 'manager'@'localhost';

-- 5.- Comando a ejecutar desde el terminal: mysql -u root < create_database.sql