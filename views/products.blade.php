@extends('layouts.base')
@section('title')
    {{$title}}
@endsection
@section('header')
    {{$header}}
@endsection
@section('content')
    <table class="table table-striped table-hover">
        <thead>
        <tr class="text-center">
            <th scope="col">Código</th>
            <th scope="col">Nombre</th>
            <th scope="col">Nombre corto</th>
            <th scope="col">Precio</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $item)
            <tr class="text-center">
                <th scope="row">{{$item->id}}</th>
                <td>{{$item->name}}</td>
                <td>{{$item->short_name}}</td>
                @if($item->price>100)            
                    <td class='text-danger'>{{number_format($item->price, 2, ",", ".")}} &euro;</td>
                @else
                    <td class='text-success'>{{number_format($item->price, 2, ",", ".")}} &euro;</td>    
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection