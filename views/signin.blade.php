@extends('layouts.base')
@section('title')
{{$title}}
@endsection
@section('header')
{{$header}}
@endsection
@section('content')
<div class="row pt-3 justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Acceso</div>
            <div class="card-body">
                <form action="" method="POST">
                    <div class="form-group row">
                        <label for="username" class="col-md-4 col-form-label text-md-right">Nombre de usuario</label>
                        <div class="col-md-6">
                            <input type="text" id="username" class="form-control" name="username" required autofocus>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
                        <div class="col-md-6">
                            <input type="password" id="password" class="form-control" name="password" required>
                        </div>
                    </div>                   

                    <div class="col-md-6 offset-md-4">
                        @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors as $error)
                            <li class="text-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                        @endif
                    </div>

                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Acceder
                        </button>                        
                    </div>
            </div>
            </form>
        </div>
    </div>

</div>
@endsection