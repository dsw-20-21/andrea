@extends('layouts.base')
@section('title')
    {{$title}}
@endsection
@section('header')
    {{$header}}
@endsection
@section('content')    
    <div class="row pt-3">
        <div class="col-8 mx-auto col-md-4 order-md-2 col-lg-5">
            <img src="https://cdn2.iconfinder.com/data/icons/round-varieties/60/Rounded_-_High_Ultra_Colour06_-_Shopping_Bag-512.png" alt="Compras" width="300">
        </div>
        <div class="col-md-8 order-md-1 col-lg-7 text-left text-md-start">
            <h2 class="mb-3 display-1">Bienvenid@</h2>
            <p class="lead mb-4">
                Encuentra los productos que necesitas, a los mejores precios, ofrecidos directamente por nuestros comercios más competitivos en el municipio de Arona, en la isla canaria de Tenerife.
            </p>
            <div class="d-flex flex-column flex-md-row">
            @if((new Project\User)->is_logged())
                <a href="products.php" class="btn btn-lg btn-primary mb-3 me-md-3">Ver productos</a>
            @else            
                <a href="register.php" class="btn btn-lg btn-primary mb-3 me-md-3">Registro</a>
                <a href="login.php" class="btn btn-lg btn-outline-secondary ml-3 mb-3">Acceso</a>                
            @endif
            </div>
        </div>
    </div>
@endsection