<?php
namespace Project;

use PDOException;

class User extends Connection {        

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Checks whether or not a username exists in the database.
     *
     * @param string $username
     * @return boolean
     */
    public function exist($username) {
        // It should check whether or not a username exists in the system.

        // Complete code here ...
    }

    /**
     * Registers a new user in the database.
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function register($username, $password) {
        // It should register a new user in the system.

        // Complete code here ...
    }

    /**
     * Logs in a user in the system.
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function login($username, $password) {
        // It should identify the user in the system. 
        // If the user is correctly identified, a "logged" cookie will be generated.

        // Complete code here ...
    }

    /**
     * Logs in a user in the system.
     *
     * @return void
     */
    public function logout() {        
        // It should logout the user from the system.
        // The "logged" cookie must be destroyed.
        
        // Complete code here ...        
    }
    
    /**
     * Checks whether or not a user is logged in.
     *
     * @return boolean
     */
    public function is_logged() {
        // It should check if the user is logged in or not. Returning "true" or "false".
        // The value of the "logged" cookie must be checked.
        
        // Complete code ...
    }    
}