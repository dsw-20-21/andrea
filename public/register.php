<?php
require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Project\User;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$title = "Registro en Comercios de Arona";
$header = "Registro en Comercios de Arona";

$errors = [];

if (!empty($_POST)) {    
    if (empty($_POST['username'])) {
        $errors[] = "Nombre de usuario no válido.";
    }

    if (empty($_POST['password1'])) {
        $errors[] = "Contraseña no válida.";
    }

    if ($_POST['password1'] != $_POST['password2']) {
        $errors[] = "Las contraseñas no coinciden.";
    }

    if (empty($errors)) {
        $user = new User();    

        if ($user->register($_POST["username"], $_POST["password1"])) {
            $user->login($_POST["username"], $_POST["password1"]);
            header('Location: products.php');
            exit;
        } else {
            $errors[] = "No fue posible registrar el usuario.";
        }
    }    
}

echo $blade->view()->make('signup', compact('title', 'header', 'errors'))->render();
