<?php
require '../vendor/autoload.php';

use Project\User;

(new User())->logout();

header('Location: index.php');

exit;