<?php
require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Project\User;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$title = "Acceso a Comercios de Arona";
$header = "Acceso a Comercios de Arona";

$errors = [];

if (!empty($_POST)) {    
    if (empty($_POST['username'])) {
        $errors[] = "Nombre de usuario no válido.";
    }

    if (empty($_POST['password'])) {
        $errors[] = "Contraseña no válida.";
    }    

    if (empty($errors)) {
        $user = new User();

        if ($user->login($_POST["username"], $_POST["password"])) {
            header('Location: login.php');
            exit;
        } else {            
            $errors[] = "Nombre de usuario o contraseña no válidos.";
        }
    }    
}

echo $blade->view()->make('signin', compact('title', 'header', 'errors'))->render();