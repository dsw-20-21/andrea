<?php
require '../vendor/autoload.php';

use Philo\Blade\Blade;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

$title = "Comercios de Arona";
$header = "Comercios de Arona";

echo $blade->view()->make('index', compact('title', 'header'))->render();