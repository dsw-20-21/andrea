<?php
require '../vendor/autoload.php';

use Philo\Blade\Blade;
use Project\Product;
use Project\User;

$views = '../views';
$cache = '../cache';
$blade = new Blade($views, $cache);

if (! (new User())->is_logged()) {    
    header('Location: index.php');
}

$title = "Productos";
$header = "Listado de productos";
$products = (new Product())->getAllProducts();

echo $blade->view()->make('products', compact('title', 'header', 'products'))->render();